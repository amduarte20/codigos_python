# Código para imprimir o símbolo '#' com posicionamento de diagonal secundária
# com base no valor de n.

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the 'staircase' function below.
#
# The function accepts INTEGER n as parameter.

def staircase(n):
    for i in range(1, n+1):
        print(str('#'*i).rjust(n))
        
# OBS: O comando 'str' serve para transformar/retornar um elemento no formato
# string e o comando 'rjust' serve para alinhar à direita, o conjunto desejado. 

if __name__ == '__main__':
    n = int(input().strip())

    staircase(n)
