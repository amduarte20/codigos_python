# Código que compara, em duas matrizes, os elementos igualmente posicionados e
# retorna a quantidade de vitórias da matriz "a" (Matriz da Alice) em relação
# a matriz "b" (Matriz do Bob).

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the 'compareTriplets' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts following parameters:
#  1. INTEGER_ARRAY a
#  2. INTEGER_ARRAY b

def compareTriplets(a, b):
    p_Alice = 0
    p_Bob = 0
    for i in range(0,3):
        if a[i] > b[i]:
            p_Alice += 1
        elif a[i] < b[i]:
            p_Bob += 1
    z = [p_Alice, p_Bob]
    return z

print(compareTriplets('17 28 30','99 16 8'))
print(compareTriplets('5 6 7','3 6 10'))

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    a = list(map(int, input().rstrip().split()))

    b = list(map(int, input().rstrip().split()))

    result = compareTriplets(a, b)

    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
