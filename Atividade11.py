# Código para verificar a quantidade de velas com maior altura que pertencem a
# lista fornecida:

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the 'birthdayCakeCandles' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY candles as parameter.

def birthdayCakeCandles(candles):
    n = 0
    x_max = max(candles)
    for i in range(len(candles)):
        if candles[i] == x_max:
            n += 1
    return n

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    candles_count = int(input().strip())

    candles = list(map(int, input().rstrip().split()))

    result = birthdayCakeCandles(candles)

    fptr.write(str(result) + '\n')

    fptr.close()
