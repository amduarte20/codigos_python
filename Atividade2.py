# Código utilizado para identificar, a partir da quantidade de montanhas (U),
# a quantidade de vales (D) em relação ao nível do mar em uma caminhada. 

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the 'countingValleys' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER steps
#  2. STRING path

def countingValleys(steps, path):
    n_level = 0
    n_valley = 0
    if 2 <= steps <= 1000000:
        print("Ok, sua escolha está correta!!")
    else:
        print("Desculpe, mas sua escolha está errada. Insira outro valor!")
    if steps == len(path):
        print("Ok, você está respeitando a restrição.")
    else:
        print("Atenção, você está desrespeitando a restrição.")
    for i in path:
        if i == "U":
            n_level += 1
            if n_level == 0:
                n_valley +=1                
        else:
            n_level -= 1
    return n_valley

print(countingValleys(8,("UDDDUDUU")))
print(countingValleys(12,("DDUUDDUDUUUD")))

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    steps = int(input().strip())

    path = input()

    result = countingValleys(steps, path)

    fptr.write(str(result) + '\n')

    fptr.close()
