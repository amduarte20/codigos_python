# Código que imprime a soma dos elementos grandes da matriz dada.

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the 'aVeryBigSum' function below.
#
# The function is expected to return a LONG_INTEGER.
# The function accepts LONG_INTEGER_ARRAY ar as parameter.

def aVeryBigSum(ar):
    s = 0
    for i in ar:
        s = s + i
    return int(s)
    print(aVeryBigSum('1000000001 1000000002 1000000003 1000000004 1000000005'))

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    ar_count = int(input().strip())

    ar = list(map(int, input().rstrip().split()))

    result = aVeryBigSum(ar)

    fptr.write(str(result) + '\n')

    fptr.close()
