# Código que retorna a soma mínima e máxima do conjunto existente:

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the 'miniMaxSum' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.

def miniMaxSum(arr):
    for i in arr:
        x = sum(arr)
        y = x - max(arr)
        z = x - min(arr)
    print (y, z)

if __name__ == '__main__':

    arr = list(map(int, input().rstrip().split()))

    miniMaxSum(arr)
