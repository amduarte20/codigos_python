# Código que calcula as proporções de elementos positivos, negativos e nulos
# e imprima o valor de cada fração em uma nova linha com 6 casas decimais. 

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the 'plusMinus' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.

def plusMinus(arr):
    n = len(arr)
    n_p = 0 # Número de valores positivos
    n_n = 0 # Número de valores negativos
    n_z = 0 # Número de valores zero
    for i in arr:
        if i > 0:
            n_p += 1
            f_p = n_p / n
        elif i < 0:
            n_n += 1
            f_n = n_n / n
        elif i == 0:
            n_z += 1
            f_z = n_z / n
            
    print("%.6f" % f_p)
    print("%.6f" % f_n)
    print("%.6f" % f_z)

#print(plusMinus([-4, 3, -9, 0, 4, 1]))
#print(plusMinus([1, 2, 3, -1, -2, -3, 0, 0]))

if __name__ == '__main__':
    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    plusMinus(arr)
