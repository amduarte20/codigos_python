# Código para determinar quantos pares de meias com cores correspondentes
# existem em determinada matriz.

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the 'sockMerchant' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER n
#  2. INTEGER_ARRAY ar

def sockMerchant(n, ar):
    n_pares = 0
    if n>=1 and n<=100:
        print("Ok, o valor inserido atende a restricao.")
    else:
        print("Atencao, o valor nao atende a restricao, insira um valor entre 1 e 100!!")
    if n == len(ar):
        print("Perfeito, tamanho correto do Array")
    else:
        print("Incorreto, insira um array com tamanho entre 1 e 100!!")
    for n in set(ar):
        n_pares += ar.count(n) // 2
    return n_pares

result = sockMerchant(10,[1, 1, 3, 1, 2, 1, 3, 3, 3, 3])
print(result)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    ar = list(map(int, input().rstrip().split()))

    result = sockMerchant(n, ar)

    fptr.write(str(result) + '\n')

    fptr.close()
