# Código para calcular o fatorial de um número inteiro (n):

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the 'extraLongFactorials' function below.
#
# The function accepts INTEGER n as parameter.

def extraLongFactorials(n):
    fat = 1
    if 1 <= n <= 100:
        print("Ok, you are respecting the constraints.")
    else:
        print("Atention.It isn't possible to calculate this value.")
        
    for i in range(1, n+1):
        fat *= i
    print(fat)
                    
if __name__ == '__main__':
    n = int(input().strip())

    extraLongFactorials(n)
