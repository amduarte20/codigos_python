# Código que imprime a soma dos elementos de um array (uma matriz):

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the 'simpleArraySum' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY ar as parameter.

def simpleArraySum(ar):
    A = sum(ar)
    return A
    n = len(ar)
    if 0 < n <= 1000 and 0 < ar[i] <= 1000:
        print("Ok, você está respeitando a restrição.")
    else:
        print("Atenção, tente novamente porque você inseriu um valor errado.")
    for i in ar:
        s = sum(ar)
        return s
    print(simpleArraySum('1 2 3 4 10 11'))

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    ar_count = int(input().strip())

    ar = list(map(int, input().rstrip().split()))

    result = simpleArraySum(ar)

    fptr.write(str(result) + '\n')

    fptr.close()
