# Código que imprime o números de letras "a" presente nas primeiras n letras de
# uma string infinita.

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the 'repeatedString' function below.
#
# The function is expected to return a LONG_INTEGER.
# The function accepts following parameters:
#  1. STRING s
#  2. LONG_INTEGER n


def repeatedString(s, n):
    A = s.count("a") # Número de "a" para serem contados na primeira string.
    B = int(n/len(s)) # Número de repetições da string.
    C = s[0:n%len(s)].count("a") # Número de "a" contados na última string.
    if 1 <= len(s) <= 100 and 1 <= n <= (10^12):
        print("Ok, sua escolha está correta.")
    else:
        print("Atenção, sua escolha está incorreta. Insira outro valor.")
    return A * B + C
print(repeatedString("aba",10))
print(repeatedString("a",1000000000000))

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    n = int(input().strip())

    result = repeatedString(s, n)

    fptr.write(str(result) + '\n')

    fptr.close()
